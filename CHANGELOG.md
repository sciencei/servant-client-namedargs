# Revision history for servant-client-named

## 0.1.0.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.

## 0.1.1.0 -- 2019-02-25

* Introduction of NamedBody' combinator

## 0.1.1.1 -- 2019-03-22

* Minor changes to support servant 0.16
